public with sharing class cl_DataTable 
{
    @AuraEnabled
    public static List<Contact> getContactList()
    {
       return [Select Id,FirstName,LastName,Phone,Email from Contact Limit 10];
    }
}
