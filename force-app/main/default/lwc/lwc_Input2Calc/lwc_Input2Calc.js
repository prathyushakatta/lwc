import { LightningElement,track } from 'lwc';

export default class Lwc_Input2Calc extends LightningElement 
{
  @track resvalue;
  num1;
  num2;
   
  callme(event)
  {
      const evtname = event.targrt.name;
      if(evtname=='aval')
      {
        this.num1=event.target.value
      }
    else
    {
       this.num2=event.target.value 
    }
  }
  addme()
  {
      const a = parseInt(this.num1);
      const b = parseInt(this.num2);
      this.resvalue='Sum of given number is' + (a+b);


  }
  sub()
  {
      const a = parseInt(this.num1);
      const b = parseInt(this.num2);
      this.resvalue='Subtraction of given number is' + (a-b);


  }
  mul()
  {
      const a = parseInt(this.num1);
      const b = parseInt(this.num2);
      this.resvalue='Multipliction of given number is' + (a*b);


  }
}